import Game from './scripts/Game'; // основной класс, собственно игра

if (!process.env.DEBUG) {
  console.log = () => {};
}

Promise.prototype.thenWait = function thenWait(time) {
  return this.then((result) => new Promise((resolve) => setTimeout(resolve, time, result)));
};

window.onload = () => {
  window.game = new Game();
};
