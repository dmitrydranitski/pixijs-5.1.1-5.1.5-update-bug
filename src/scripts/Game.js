import ResourceManager from './services/ResourceManager';
import BaseSprite from './sprites/BaseSprite';

class Game {
  constructor() {
    console.log('Game before ResourceManager.init');
    ResourceManager.init(() => this.onLoad());
    console.log('Game after ResourceManager.init');
  }

  onLoad() {
    this.app = new PIXI.Application({width: 900, height: 900, resolution: 1});
    this.app.renderer.backgroundColor = 0xFFFFFF;

    console.log('Game onLoad start');

    document.body.appendChild(this.app.view);

    // ------ GIF

    // 1. gif by file
    this.app.stage.addChild(new PIXI.Text('1. gif by file', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(30, 130);
    const test1 = this.app.stage.addChild(new BaseSprite('gif-file'));
    test1.position.set(30, 30);
    test1.anchor.set(0, 0);

    // 2. gif by base64
    this.app.stage.addChild(new PIXI.Text('2. gif by base64', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(230, 130);
    const test2 = this.app.stage.addChild(new BaseSprite('gif-base64'));
    test2.position.set(230, 30);
    test2.anchor.set(0, 0);

    // 3. png by file
    this.app.stage.addChild(new PIXI.Text('3. png by file', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(430, 130);
    const test3 = this.app.stage.addChild(new BaseSprite('png-file'));
    test3.position.set(430, 30);
    test3.anchor.set(0, 0);

    // 4. png by base64
    this.app.stage.addChild(new PIXI.Text('4. png by base64', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(630, 130);
    const test4 = this.app.stage.addChild(new BaseSprite('png-base64'));
    test4.position.set(630, 30);
    test4.anchor.set(0, 0);

    // 5. small spritesheet by base64 and js
    this.app.stage.addChild(new PIXI.Text('5. small spritesheet by base64 and js', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(30, 330);
    const test5 = this.app.stage.addChild(new BaseSprite('accessories-dictionary'));
    test5.position.set(30, 230);
    test5.anchor.set(0, 0);

    // 6. small spritesheet by base64 and json
    this.app.stage.addChild(new PIXI.Text('6. small spritesheet by base64 and json', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(450, 330);
    const test6 = this.app.stage.addChild(new BaseSprite('naccessories-dictionary'));
    test6.position.set(450, 230);
    test6.anchor.set(0, 0);

    // 7. big spritesheet by base64 and json
    this.app.stage.addChild(new PIXI.Text('7. big spritesheet by base64 and json', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(30, 530);
    const test7 = this.app.stage.addChild(new BaseSprite('snake'));
    test7.scale.set(0.1, 0.1);
    test7.position.set(30, 400);
    test7.anchor.set(0, 0);

    // 9. one image spritesheet by base64 and json
    this.app.stage.addChild(new PIXI.Text('9. one image spritesheet by base64 and json', {fontFamily: 'Arial', fontSize: 24, fill: 0x000000, align: 'center'})).position.set(30, 730);
    const test9 = this.app.stage.addChild(new BaseSprite('xxxxxWebp.net-resizeimage-retina'));
    test9.scale.set(0.2, 0.2);
    test9.position.set(30, 600);
    test9.anchor.set(0, 0);
  }
}

export default Game;
