import ResourceManager from '../services/ResourceManager';

class BaseSprite extends PIXI.Sprite {
  constructor(textureId) {
    const resourceManager = ResourceManager.init();
    const baseTexture = resourceManager.getTexture(textureId);
    super(baseTexture);

    this.anchor.set(0.5, 0.5);

    this.resourceManager = resourceManager;
  }

  destroy() {
    super.destroy({children: true});
    if (this.parent) {
      this.parent.removeChild(this);
    }
  }
}

export default BaseSprite;
