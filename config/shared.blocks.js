const path = require('path');
const webpack = require('webpack');
const {group, entryPoint} = require('@webpack-blocks/webpack2');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname, '..');
const DIST_PATH = path.resolve(ROOT_PATH, 'dist');
const SRC_PATH = path.resolve(ROOT_PATH, 'src');
const ZIP_PATH = path.resolve(ROOT_PATH, 'zip');

const entrypoint = () => entryPoint(`${SRC_PATH}/index.js`);

const output = () => () => ({
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(DIST_PATH),
  },
});

const babelLoader = () => () => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env'],
          },
        },
      },
    ],
  },
});

const fileLoader = () => () => ({
  module: {
    rules: [
      {
        test: /\.(jpg|png|svg|ttf|otf)$/,
        use: {
          loader: 'url-loader',
        },
      },
    ],
  },
});

const createHtml = () => () => ({
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: `${SRC_PATH}/index.template.html`,
    }),
  ],
});

const fs = () => () => ({
  node: {
    fs: 'empty',
  },
});

const defineEnvVariables = () => () => ({
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'LOCALE': JSON.stringify(process.env.LOCALE && process.env.LOCALE.toLowerCase()),
        'DEFAULT_LOCALE': JSON.stringify(process.env.DEFAULT_LOCALE.toLowerCase()),
        'DEBUG': JSON.stringify(process.env.DEBUG && process.env.DEBUG.toLowerCase()),
      },
    }),
  ],
});

const providePlugin = () => () => ({
  plugins: [
    new webpack.ProvidePlugin({
      PIXI: 'pixi.js',
      // Sentry: '@sentry/browser',
    })],
});

const basicConfig = () => (
  group([
    entrypoint(),
    output(),
    babelLoader(),
    fileLoader(),
    createHtml(),
    fs(),
    defineEnvVariables(),
    providePlugin(),
  ])
);

module.exports = {
  basicConfig,
  DIST_PATH,
  ZIP_PATH,
};
