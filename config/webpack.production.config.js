const {DIST_PATH, ZIP_PATH, basicConfig} = require('./shared.blocks.js');
const {createConfig} = require('@webpack-blocks/webpack2');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackZipPlugin = require('webpack-zip-plugin');

const prodScssToCss = () => () => ({
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: 'css-loader',
            options: {
              minimize: true,
            },
          }, {
            loader: 'sass-loader',
          }, {
            loader: 'resolve-url-loader',
          }],
        }),
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].bundle.css',
    }),
  ],
});

const uglifyJs = () => () => ({
  plugins: [
    new UglifyJSPlugin(),
  ],
});

const zip = () => () => ({
  plugins: [
    new WebpackZipPlugin({
      initialFile: DIST_PATH,
      endPath: ZIP_PATH,
      zipName: `${process.env.LOCALE || process.env.DEFAULT_LOCALE}_${process.env.TYPE}.zip`,
    }),
  ],
});

module.exports = createConfig([
  basicConfig(),
  prodScssToCss(),
  uglifyJs(),
  zip(),
]);
